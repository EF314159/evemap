package EVERender;

public class VoronoiBuilder extends Thread {
    private Galaxy g;
    private EveRender main;
    private volatile boolean terminate = false;
    
    public VoronoiBuilder(Galaxy g, EveRender main) {
        this.g = g;
        this.main = main;
    }

    /**
     * The method executed by Thread. Scrapes webpages for data and modifies
     * the instance of Galaxy passed to APIScraper in the constructor.
     */
    @Override public void run() {
        for (Star s : g.getStars()) {
            if (terminate) return;
            g.makeVoronoiCell(s);
        }
        
        main.writeVoronoiDiagram();
    }
    
    /**
     * Instructs this thread to terminate as soon as possible.
     */
    public void terminate() {
        terminate = true;
    }
}
