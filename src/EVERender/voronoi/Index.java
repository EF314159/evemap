package EVERender.voronoi;

public class Index {
    int num;
    
    public Index(int num) {
        this.num = num;
    }
    
    @Override public String toString() {
        return String.valueOf(num);
    }
}
