/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EVERender.voronoi;

import com.jme3.math.Vector3f;

/**
 *
 * @author sadistictribble
 */
public class Plane {
    Vector3f point, normal;

    public Plane(Vector3f point, Vector3f normal) {
        this.point = point;
        this.normal = normal;
    }

    public boolean isBehindPlane(Vector3f v) {
        // get difference between v and the point on the plane
        Vector3f dst = v.subtract(point);

        // dot the difference by the plane normal
        float dot = dst.dot(normal);

        // since mesh normals face outward, v is behind the plane if the
        // dot product is positive
        return (dot > 0);
    }
    
    public boolean intersectsEdge(Vector3f v1, Vector3f v2) {
        return isBehindPlane(v1) ^ isBehindPlane(v2);
    }
    
    public Vector3f cutEdge(Vector3f v1, Vector3f v2) {
        Vector3f front, behind;
        if (isBehindPlane(v1)) {
            behind = v1; front = v2;
        } else {
            behind = v2; front = v1;
        }

        Vector3f direction = behind.subtract(front).normalize();

        float distance = 
                point.subtract(front).dot(normal) /
                direction.dot(normal);

        behind = front.add(direction.mult(distance));
        
        return behind;
    }
}
