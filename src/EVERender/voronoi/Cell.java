package EVERender.voronoi;

import EVERender.Star;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.util.BufferUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Represents a single Voronoi cell in the map's 3D Voronoi diagram.
 */
public class Cell {
    private final Star center;
    private List<Poly> mesh;
    public List<Vector3f> verts;
    
    public Cell(Star center, Collection<Star> stars) {
        this.center = center;
        
        makeBoundingBox(.5f, .15f, .5f);
        
        for (Star s : stars) {
            if (s != center) {
                cut(s);
            }
        }
    }
    
    public Cell(Star center, Map<Integer, Star> stars, String input) {
        String[] lines = input.split("[\r\n]+");
        
        for (String line : lines) {
            int commentIndex = line.indexOf("//");
            if (commentIndex >= 0) line = line.substring(0, commentIndex);
            
            String[] tokens = line.split("[ \t]");
            if (tokens[0].toLowerCase().equals("v")) {
                if (tokens.length < 4) continue;
                
                verts.add(new Vector3f(
                        Float.parseFloat(tokens[1]),
                        Float.parseFloat(tokens[2]),
                        Float.parseFloat(tokens[3])));
                
            } else if (tokens[0].toLowerCase().equals("p")) {
                if (tokens.length < 5) continue;
                
                List<Index> indices = new LinkedList<Index>();
                
                //mesh.add(new Poly());
            }
        }
        
        this.center = center;
    }
    
    /**
     * After cutting, Cells can be left with many vertices that exist in the 
     * verts list but are not used on any polygon. To save memory and space 
     * when serializing, these are removed.
     */
    private void removeUnusedVerts() {
        List<Integer> toRemove = new LinkedList<Integer>();
        for (int i = verts.size()-1; i >= 0; i--) {
            boolean remove = true;
            for (Poly p : mesh) {
                if (p.containsVertex(i)) {
                    remove = false;
                    break;
                }
            }
            if (remove) toRemove.add(i);
        }
        
        for (int i : toRemove) {
            removeVert(i);
        }
    }
    
    /**
     * Removes a single vertex from the list of verts. Adjusts all polygon
     * indices to match the new indices.
     * 
     * @param index the index of the vertex to remove
     */
    private void removeVert(int index) {
        verts.remove(index);
        
        for (Poly p : mesh) {
            p.remove(index);
        }
    }
    
    /**
     * Outputs this Cell's 
     * @return 
     */
    @Override public String toString() {
        String out = "";
        for (Vector3f v : verts) {
            out += "v " + v.x + " " + v.y + " " + v.z + "\n";
        }
        for (Poly p : mesh) {
            out += p.toString() + "\n";
        }
        return out;
    }
    
    /**
     * Given a Poly and two vertex indices representing an edge, finds another
     * Poly in this Cell containing that edge. Will never return the given Poly.
     * If no other Poly contains the given edge, returns null.
     * 
     * @param poly the Poly not to return
     * @param i1 an index of the edge
     * @param i2 the other index of the edge
     * @return another Poly in this Cell containing the given edge
     */
    public Poly findOther(Poly poly, int i1, int i2) {
        for (Poly p : mesh) {
            if (poly != p && p.containsEdge(i1, i2)) return p;
        }
        
        return null;
    }
    
    /**
     * Polygons in a cell are cut by a plane by first finding a single Poly
     * that intersects the plane, and finding one of the two lines in that Poly
     * that intersect the plane. This line is the first and last cut to be
     * made. A Plane intersecting a convex polygon must intersect through
     * exactly two edges, so an algorithm can "walk" the mesh from one edge to
     * the other until arriving back at the first edge. Since voronoi cells are
     * convex hulls, this is sufficient to cut a cell by a plane.
     * 
     * @param otherStar the Star on the opposite side of the plane
     */
    public void cut(Star otherStar) {
        Vector3f halfwayPoint = new Vector3f(
                (center.location.x + otherStar.location.x)/2,
                (center.location.y + otherStar.location.y)/2,
                (center.location.z + otherStar.location.z)/2);
        
        // normal points outward
        Vector3f normal = new Vector3f(
                otherStar.location.x - halfwayPoint.x,
                otherStar.location.y - halfwayPoint.y,
                otherStar.location.z - halfwayPoint.z);
        
        Plane p = new Plane(halfwayPoint, normal);
        
        // find an edge
        Index[] firstEdge;
        
        // storage for the new Poly that is created if the plane intersects
        // this Cell
        Poly newPoly = null;
        
        for (Poly poly : mesh) {
            // find an edge to start with
            firstEdge = poly.intersectsPlane(p);
            
            if (firstEdge != null) {
                // cut the first edge
                Vector3f firstVert = p.cutEdge(
                        verts.get(firstEdge[0].num),
                        verts.get(firstEdge[1].num));
                verts.add(firstVert);
                
                // get the cut vertex's index
                List<Integer> newVerts = new LinkedList<Integer>();
                int firstVertIndex = verts.indexOf(firstVert);
                
                // add the cut vertex to the new Poly to start it off
                newVerts.add(firstVertIndex);
                
                // the cut() algorithm populates newVerts while walking the mesh
                poly.cut(p, newVerts,
                        firstVertIndex, firstEdge[0].num, firstEdge[1].num,
                        firstVertIndex, firstEdge[0].num, firstEdge[1].num);
                
                // create the new Poly from the collected verts
                newPoly = new Poly(this, newVerts, otherStar);
                
                // if the new Poly's normal is backwards, flip it
                if (newPoly.getNormal().dot(p.normal) < 0) {
                    newPoly.flip();
                }
                
                // only start the algorithm once, of course
                break;
            }
        }
        List<Poly> toRemove = new LinkedList<Poly>();
        
        for (Poly poly : mesh) {
            if (poly.isBehindPlane(p)) toRemove.add(poly);
        }
        for (Poly poly : toRemove) {
            mesh.remove(poly);
        }
        
        if (newPoly != null) {
            mesh.add(newPoly);
        }
    }
    
    /**
     * Triangulates all the polygons of this Cell.
     */
    public void triangulate() {
        List<Poly> toAdd = new LinkedList<Poly>();
        
        for (Poly p : mesh) {
            toAdd.addAll(p.triangulate());
        }
        
        for (Poly p : toAdd) {
            mesh.add(p);
        }
    }
    
    /**
     * Removes polygons on borders for which the neighboring Star's
     * voronoiColor is the same as the center's voronoiColor.
     */
    public void cleanBorders() {
        List<Poly> toRemove = new LinkedList<Poly>();
        for (Poly p : mesh) {
            if (p.border == null || p.border.sovHolder == center.sovHolder) {
                toRemove.add(p);
            }
        }
        for (Poly p : toRemove) mesh.remove(p);
    }
    
    /**
     * Converts this model into a JMonkeyEngine Geometry.
     * 
     * @return the JME Geometry
     */
    public Geometry getJMEModel() {
        Mesh JMEmesh = new Mesh();
        
        removeUnusedVerts();
        triangulate();
        
        Vector3f[] vertArray = new Vector3f[verts.size()];
        Vector4f[] colorArray = new Vector4f[verts.size()];
        int[] indices = new int[mesh.size()*3*2];
        
        boolean hasBorderPolies = false;
        for (Poly p : mesh) {
            if (p.border != null &&
                    p.border.sovHolder != center.sovHolder) {
                hasBorderPolies = true;
            }
        }
        
        for (int i = 0; i < verts.size(); ++i) {
            vertArray[i] = verts.get(i);
            
            float colorMultiplier = (.075f - center.location.distance(verts.get(i)))*10;
            colorMultiplier = FastMath.clamp(colorMultiplier, 0, 1);
            
            if (!hasBorderPolies) colorMultiplier *= 0.125f;
            else {
                boolean isBorderVertex = false;
                for (Poly p : mesh) {
                    if (p.containsVertex(i) && 
                            p.border != null &&
                            p.border.sovHolder != center.sovHolder) {
                        isBorderVertex = true;
                    }
                }
                if (!isBorderVertex) colorMultiplier *= 0.05f;
            }
            
            colorArray[i] = new Vector4f(
                    center.voronoiColor.x * colorMultiplier,
                    center.voronoiColor.y * colorMultiplier,
                    center.voronoiColor.z * colorMultiplier,
                    1);
        }
        
        int i = 0;
        for (Poly p : mesh) {
            int[] polyIndices = p.getIndices();
            if (polyIndices.length < 3) continue;
            
            indices[i++] = polyIndices[0];
            indices[i++] = polyIndices[1];
            indices[i++] = polyIndices[2];
            
            indices[i++] = polyIndices[0];
            indices[i++] = polyIndices[2];
            indices[i++] = polyIndices[1];
        }
        
        JMEmesh.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(vertArray));
        JMEmesh.setBuffer(Type.Color,    4, BufferUtils.createFloatBuffer(colorArray));
        JMEmesh.setBuffer(Type.Index,    3, BufferUtils.createIntBuffer(indices));
        JMEmesh.updateBound();
        
        Geometry geo = new Geometry("VoronoiCell", JMEmesh);
        
        return geo;
    }
    
    /**
     * Sets up the initial bounding box given a thickness in x, y and z. The
     * 
     * @param x the x-axis size of the box
     * @param y the y-axis size of the box
     * @param z the z-axis size of the box 
     */
    private void makeBoundingBox(float x, float y, float z) {
        mesh = new LinkedList<Poly>();
        verts = new ArrayList<Vector3f>();
        
        verts.add(new Vector3f(-x, -y, -z));
        verts.add(new Vector3f( x, -y, -z));
        verts.add(new Vector3f( x, -y,  z));
        verts.add(new Vector3f(-x, -y,  z));
        verts.add(new Vector3f(-x,  y, -z));
        verts.add(new Vector3f( x,  y, -z));
        verts.add(new Vector3f( x,  y,  z));
        verts.add(new Vector3f(-x,  y,  z));
        
        mesh.add(new Poly(this, new int[]{
            3, 2, 1, 0
        }, null));
        mesh.add(new Poly(this, new int[]{
            4, 5, 6, 7
        }, null));
        mesh.add(new Poly(this, new int[]{
            0, 1, 5, 4
        }, null));
        mesh.add(new Poly(this, new int[]{
            1, 2, 6, 5
        }, null));
        mesh.add(new Poly(this, new int[]{
            2, 3, 7, 6
        }, null));
        mesh.add(new Poly(this, new int[]{
            3, 0, 4, 7
        }, null));
    }
}
