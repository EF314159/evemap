/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EVERender.voronoi;

import EVERender.Star;
import com.jme3.math.Vector3f;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a single surface in a 3D voronoi cell, either set by initial
 * conditions or made by cutting an existing cell by a plane.
 */
public class Poly {
    // the containing cell
    private Cell cell;
    // the list of verts
    private List<Index> indices;
    // the Star whose plane made this cut
    public final Star border;
    // multiple of the rendered brightness of this Poly, set by the containingCell
    public float brightness = 1;
    
    /**
     * Creates a Poly from a list of indices.
     * 
     * Index list is assumed to be valid: indices are in either clockwise or
     * counterclockwise order, there are three or more indices, indices all lie
     * in the same plane.
     * 
     * @param cell the containing cell
     * @param indices the list of indices
     */
    public Poly(Cell cell, List<Integer> indices, Star border) {
        this.cell = cell;
        this.indices = new ArrayList<Index>();
        for (int i : indices) {
            this.indices.add(new Index(i));
        }
        this.border = border;
    }
    
    /**
     * Creates a Poly from an array of indices. Inserts each index into this
     * Poly's list.
     * 
     * Index list is assumed to be valid: indices are in either clockwise or
     * counterclockwise order, there are three or more indices, indices all lie
     * in the same plane.
     * 
     * @param cell the containing cell
     * @param indices the index array
     */
    public Poly(Cell cell, int[] indices, Star border) {
        this.cell = cell;
        this.indices = new LinkedList<Index>();
        this.border = border;
        for (int i = 0; i < indices.length; ++i) {
            this.indices.add(new Index(indices[i]));
        }
    }

    /**
     * Returns true if some of this cell lies behind the plane and some of it
     * does not. This Poly should be cut iff this method returns true.
     * 
     * @param p the plane to check for intersection
     * @return true if this Poly intersects the plane
     */
    public Index[] intersectsPlane(Plane p) {
        for (Index i : indices) {
            Index i2 = next(i);
            
            if (p.intersectsEdge(cell.verts.get(i.num), cell.verts.get(i2.num))) {
                return new Index[]{i, i2};
            }
        }

        return null;
    }
    
    /**
     * Returns true of this Poly lies entirely behind the plane. This Poly
     * should be removed from the cell iff this method returns true.
     * 
     * @param p the plane to check for intersection
     * @return true if this Poly is behind the plane
     */
    public boolean isBehindPlane(Plane p) {
        for (Index i : indices) {
            if (!p.isBehindPlane(cell.verts.get(i.num))) return false;
        }
        return true;
    }
    
    /**
     * Polygons in a cell are cut by a plane by first finding a single Poly
     * that intersects the plane, and finding one of the two lines in that Poly
     * that intersect the plane. This line is the first and last cut to be
     * made. A Plane intersecting a convex polygon must intersect through
     * exactly two edges, so an algorithm can "walk" the mesh from one edge to
     * the other until arriving back at the first edge. Since voronoi cells are
     * convex hulls, this is sufficient to cut a cell by a plane.
     * 
     * Cut edges are passed as two indices, representing the endpoints, as well
     * as the index of a new vert on the plane created by cutting the edge.
     * 
     * Starting from the first line, the cut() method:
     *  - finds the other line in this Poly intersected by the plane
     *  - store this "opposite edge"
     *  - entirely remove any verts behind the plane that are not part of
     *      either the starting or opposite edge
     *  - cut the starting edge
     *  - cut the opposite edge (by replacing it with a new edge)
     *  - if the opposite edge is not the final edge,
     *      - store the new vertex from the cut of the starting edge, since it
     *          must be added to a new Poly on the surface of the cut. Since
     *          the algorithm "walks" the surface of the mesh, this new list is
     *          always in some order; either clockwise or anticlockwise.
     *      - find the other Poly which shares the opposite edge and recurse,
     *          passing along the final edge and using this opposite edge as
     *          the next starting edge
     * 
     * @param p the Plane by which to cut
     * @param newVerts the list storing the new vertex indices
     * @param startVert the new, cut vertex index from the starting edge
     * @param startEdge1 a vertex index on the starting edge
     * @param startEdge2 the other vertex index on the starting edge
     * @param lastVert the new, cut vertex on teh last edge
     * @param lastEdge1 a vertex index on the last edge
     * @param lastEdge2 the other vertex index on the last edge
     */
    public void cut(Plane p, List<Integer> newVerts,
            int startVert, int startEdge1, int startEdge2,
            int lastVert, int lastEdge1, int lastEdge2) {
        
        // TODO: clean up all these fucking variables
        //System.out.println();
        //System.out.println("CUTTING  " + toString() + 
        //        " STARTING FROM (" + startEdge1 + " " + startEdge2 + ") " + startVert + "  " +
        //        " ENDING AT (" + lastEdge1 + " " + lastEdge2 + ")");
        
        // create a copy of the starting edge, we'll need the original indices
        Index startEdgeCpy1 = null, startEdgeCpy2 = null,
                newEdge1 = null, newEdge2 = null;
        
        // find opposite edge, copy starting edge
        for (Index i : indices) {
            Index i2 = next(i);
            if (p.intersectsEdge(cell.verts.get(i.num), cell.verts.get(i2.num))) {
                if (edgeEquals(i.num, i2.num, startEdge1, startEdge2)) {
                    startEdgeCpy1 = i;
                    startEdgeCpy2 = i2;
                } else {
                    newEdge1 = i;
                    newEdge2 = i2;
                }
            }
        }
        // We now assume that both the startEdgeCpys and newEdges are not null,
        // since a plane must intersect a convex polygon through exactly two
        // edges. If they're null, the algorithm is FUBAR anyway and JME will
        // display the exception.
        
        // remove vertices not on the starting/opposite edge that are behind
        // the plane
        List<Index> toRemove = new LinkedList<Index>();
        for (Index i : indices) {
            if (i.num != startEdge1 && i.num != startEdge2 && i != newEdge1 && i != newEdge2) {
                if (p.isBehindPlane(cell.verts.get(i.num))) {
                    //System.out.println("remove " + i.num);
                    toRemove.add(i);
                }
            }
        }
        for (Index i : toRemove) indices.remove(i);
        
        // copy the opposite edge, we'll need the original indices
        Index oppEdge1 = new Index(newEdge1.num);
        Index oppEdge2 = new Index(newEdge2.num);
        Index newVert = new Index(-1);
        
        // get which index on the starting and opposite edge must be moved
        Index startVertToMove = getIndexBehindPlane(p, startEdgeCpy1, startEdgeCpy2);
        Index oppVertToMove = getIndexBehindPlane(p, newEdge1, newEdge2);
        
        if (startVertToMove == oppVertToMove) {
            // handle the case where these two indices are the same
            Index otherStartVert;
            
            // the other vertex on the starting edge is either the next or
            // previous index on the list from startVertToMove
            if (startVertToMove == startEdgeCpy1) otherStartVert = startEdgeCpy2;
            else otherStartVert = startEdgeCpy1;
            
            if (otherStartVert == next(startVertToMove)) {
                indices.add(indices.indexOf(startVertToMove)+1, new Index(startVert));
            } else {
                indices.add(indices.indexOf(startVertToMove), new Index(startVert));
            }
        } else {
            // if they're not the same just change startVertToMove
            startVertToMove.num = startVert;
        }

        // check whether this is the last Poly to be cut
        if (edgeEquals(oppEdge1.num, oppEdge2.num, lastEdge1, lastEdge2)) {
            // we already have the last cut vert and the new Poly is finished
            // being constructed - just edit the last edge and return
            oppVertToMove.num = lastVert;
            
            //System.out.println(toString() + " RETURNING");
        } else {
            // cut the next edge - get the new vertex's position, add it to the
            // cell's mesh, and get its index
            Vector3f newVertPosition = p.cutEdge(
                    cell.verts.get(newEdge1.num),
                    cell.verts.get(newEdge2.num));
            cell.verts.add(newVertPosition);
            newVert.num = cell.verts.indexOf(newVertPosition);
            
            //System.out.println("ADDING " + newVert.num + " at " + newVertPosition);

            // edit the last edge
            oppVertToMove.num = newVert.num;

            // find the Poly neighboring this one on the opposite edge
            Poly next = cell.findOther(this, oppEdge1.num, oppEdge2.num);
            
            // keep building the new Poly for the surface
            newVerts.add(newVert.num);

            //System.out.println(toString() + " -> NEXT  " + next.toString());
            //System.out.println(newVerts.toString());
            
            // recurse
            next.cut(p, newVerts,
                    newVert.num, oppEdge1.num, oppEdge2.num,
                    lastVert, lastEdge1, lastEdge2);
        }
    }
    
    /**
     * Gets the next vertex index in this Poly's list from the one provided.
     * Will wrap around the list if necessary. Returns null if i is not in this
     * Poly's indices.
     * 
     * @param i a vertex index in this Poly's indices
     * @return the next index in this Poly's indices
     */
    private Index next(Index i) {
        if (!indices.contains(i)) return null;
        else return indices.get((indices.indexOf(i)+1) % indices.size());
    }
    
    /**
     * Returns whether the edge represented by the first pair of vertex indices
     * is the same as the edge represented by the second pair. Indices can be
     * given in any order: indices 1 and 8 is the same edge as indices 8 and 1.
     * 
     * Does no checking of whether the given indices are an edge in this Poly.
     * edge_equals(1, 8, 8, 1) will return true even if there is no edge (1, 8).
     * 
     * @param v1 an index on the first edge
     * @param v2 the other index on the first edge
     * @param v3 an index on the second edge
     * @param v4 the other index on the second edge
     * @return whether the indices represent the same edge, given that they
     *  represent some edge
     */
    private boolean edgeEquals(int v1, int v2, int v3, int v4) {
        return ((v1 == v3 && v2 == v4) ||
                (v1 == v4 && v2 == v3));
    }
    
    /**
     * If one of the given vertex indices represents a vertex that is behind
     * the given Plane, returns it. If both are behind the Plane, returns the
     * forst argument. If neither are behind the Plane, returns null.
     * 
     * @param p the Plane to test against
     * @param i1 a vertex index
     * @param i2 the other vertex index
     * @return which vertex, if any, is behind the Plane
     */
    private Index getIndexBehindPlane(Plane p, Index i1, Index i2) {
            if (p.isBehindPlane(cell.verts.get(i1.num))) return i1;
            else if (p.isBehindPlane(cell.verts.get(i2.num))) return i2;
            else return null;
    }
    
    /**
     * Determines whether this Poly contains an edge, given as a pair of vertex
     * indices. The Poly contains this edge iff its index list contains the
     * given indices consecutively in either order.
     * 
     * @param edge1 an first index in the edge
     * @param edge2 the other index in the edge
     * @return whether this Poly contains the given edge
     */
    public boolean containsEdge(int edge1, int edge2) {
        for (Index i : indices) {
            Index i2 = next(i);
            
            if (edgeEquals(i.num, i2.num, edge1, edge2)) return true;
        }
        
        return false;
    }
    
    /**
     * Determines whether this Poly contains a given vertex, given by its index.
     * 
     * @param index the index to check for
     * @return whether this Poly contains the given vertex
     */
    public boolean containsVertex(int index) {
        for (Index i : indices) {
            if (i.num == index) return true;
        }
        
        return false;
    }
    
    /**
     * Gets the normal of this Polygon using the cross product of two of its
     * edges. Assumes that the sampled normal correctly represents the Poly as
     * a whole (that the Poly's vertices all lie in the same plane).
     * 
     * @return this Poly's normal
     */
    public Vector3f getNormal() {
        Index i = indices.get(0);
        
        Vector3f vert1 = cell.verts.get(i.num);
        Vector3f vert2 = cell.verts.get(next(i).num);
        Vector3f vert3 = cell.verts.get(next(next(i)).num);
        
        Vector3f edge1 = vert1.subtract(vert2);
        Vector3f edge2 = vert3.subtract(vert2);
        
        return edge1.cross(edge2);
    }
    
    /**
     * Reverses the order of the vertex indices in this Poly's list. This
     * effectively flips the polygon's normal, since vertex winding determines
     * facing in OpenGL.
     */
    public void flip() {
        List<Index> flippedIndices = new LinkedList<Index>();
        
        for (Index i : indices) {
            flippedIndices.add(0, i);
        }
        
        indices = flippedIndices;
    }
    
    /**
     * Determines whether this Poly is triangulated (true iff the number of
     * vertex indices is less than 4). This method ignores the possibility that
     * this Poly is invalid (with less than three indices).
     * 
     * @return false if this Poly has more than three vertices, true otherwise
     */
    public boolean isTriangulated() {
        return (indices.size() < 4);
    }
    
    /**
     * Triangulates this Poly by connecting the first vertex in the indices
     * array with every other vertex. This method assumes that the Poly is
     * convex. After execution, this Poly will consist of only its first three
     * vertices, and the rest of the triangles in the new "fan" will be in the
     * returned List.
     * 
     * @return a List of triangle Polies created by triangulating this one
     */
    public List<Poly> triangulate() {
        List<Poly> newPolies = new LinkedList<Poly>();
        
        while (!isTriangulated()) {
            //newPolies.add(new Poly(mesh, indices.subList(0, 3)));
            
            List<Integer> newIndices = new LinkedList<Integer>();
            newIndices.add(indices.get(0).num);
            newIndices.add(indices.get(1).num);
            newIndices.add(indices.get(2).num);
            
            newPolies.add(new Poly(cell, newIndices, border));
            
            indices.remove(1);
        }
        
        return newPolies;
    }
    
    /**
     * Returns this Poly's vertex indices as an int array.
     * 
     * @return this Poly's vertex indices as an int array
     */
    public int[] getIndices() {
        int[] toReturn = new int[indices.size()];
        
        for (int i = 0; i < toReturn.length; ++i) {
            toReturn[i] = indices.get(i).num;
        }
        
        return toReturn;
    }
    
    /**
     * Given a vertex that is not used by any polygons, adjusts the indices of
     * this Poly to account for the missing vertex. Any index greater than the
     * given index is decremented by one.
     * 
     * @param index the index of the removed vertex
     */
    public void remove(int index) {
        for (Index i : indices) {
            if (i.num > index) i.num--;
        }
    }
    
    /**
     * Constructs a parenthesized list of the indices in this Poly.
     * 
     * @return the list
     */
    @Override public String toString() {
        String out = "p " + 
                (border == null ? "-1" : border.id) +
                " ";
        for (Index i : indices) {
            out += i.num + " ";
        }
        
        return out;
    }
}
