package EVERender;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Scrapes EVE API webpages to read data for kills/jumps in star systems.
 * Extends Thread and runs alongside the render/update thread so that data can
 * be retrieved without interrupting display.
 */
public class APIScraper extends Thread {
    private Galaxy g;
    private volatile boolean terminate = false;
    
    /**
     * Creates a new instance of APIScraper. Does not attempt to get any data.
     * 
     * @param g the Galaxy to which data should be passed
     */
    public APIScraper (Galaxy g) {
        this.g = g;
    }

    /**
     * The method executed by Thread. Scrapes webpages for data and modifies
     * the instance of Galaxy passed to APIScraper in the constructor.
     */
    @Override public void run() {
        grabSovData();
        if (terminate) return;
        grabJumpData();
        if (terminate) return;
        grabKillData();
    }
    
    /**
     * Instructs this thread to terminate as soon as possible.
     */
    public void terminate() {
        terminate = true;
    }
    
    private void grabSovData() {
        boolean finished = false;
        String tempSovData = 
                "//Temporary sov data, stored in case of slow/no internet" +
                "//SolarSystemID: allianceID, corpID\n" +
                "//Both IDs replaced by faction ID in NPC space\n\n";
        
        /*try {
            URL url = new URL("https://api.eveonline.com/map/Sovereignty.xml.aspx");
            
            BufferedReader in = new BufferedReader (new InputStreamReader(url.openStream()));
        
            String line;
            while ((line = in.readLine()) != null && !terminate) {
                line = line.trim();
                if (line.startsWith("<row ")) {
                    String[] tokens = line.split("\"");
                    
                    int allianceID = Integer.parseInt(tokens[3]);
                    int factionID = Integer.parseInt(tokens[5]);
                    int corpID = Integer.parseInt(tokens[9]);
                    
                    int aID = (allianceID == 0) ? factionID : allianceID;
                    int cID = (corpID == 0) ? factionID : corpID;
                    
                    tempSovData += tokens[1] + ": " + aID + ", " + cID + "\n";
                    
                    g.setSov(Integer.parseInt(tokens[1]), aID);
                }
                
            }
            finished = true;
            in.close();
        } catch (MalformedURLException ex) {
            System.err.println("Malformed URL when scraping sov data.");
            System.err.println(ex.getMessage());
        } catch (IOException ex) {
            System.err.println("IOException when scraping for sov data.");
            System.err.println(ex.getMessage());
        }*/
        
        g.buildVoronoiDiagram();
        if (finished) writeOut("temp_sov.txt", tempSovData);
    }
    
    private void grabJumpData() {
        boolean finished = false;
        String tempJumpData = 
                "//Temporary jump data, stored in case of slow/no internet" +
                "//SolarSystemID: jumps\n\n";
        
        try {
            URL url = new URL("https://api.eveonline.com/map/Jumps.xml.aspx");
            
            BufferedReader in = new BufferedReader (new InputStreamReader(url.openStream()));
        
            String line;
            while ((line = in.readLine()) != null && !terminate) {
                line = line.trim();
                if (line.startsWith("<row ")) {
                    String[] tokens = line.split("\"");
                    
                    g.setJumpsPerHour(
                            Integer.parseInt(tokens[1]),
                            Integer.parseInt(tokens[3]));
                    tempJumpData += tokens[1] + ": " + tokens[3] + "\n";
                }
            }
            
            finished = true;
            in.close();
        } catch (MalformedURLException ex) {
            System.err.println("Malformed URL when scraping jump data.");
            System.err.println(ex.getMessage());
        } catch (IOException ex) {
            System.err.println("IOException when scraping for jump data.");
            System.err.println(ex.getMessage());
        }
        
        if (finished) writeOut("temp_jumps.txt", tempJumpData);
    }
    
    private void grabKillData() {
        boolean finished = false;
        String tempKillData = 
                "//Temporary kill data, stored in case of slow/no internet" +
                "//SolarSystemID: jumps\n\n";
        
        try {
            URL url = new URL("https://api.eveonline.com/map/Kills.xml.aspx");
            
            BufferedReader in = new BufferedReader (new InputStreamReader(url.openStream()));
        
            String line;
            while ((line = in.readLine()) != null && !terminate) {
                line = line.trim();
                if (line.startsWith("<row ")) {
                    String[] tokens = line.split("\"");
                    
                    g.setKillsPerHour(
                            Integer.parseInt(tokens[1]),
                            Integer.parseInt(tokens[3]));
                    tempKillData += tokens[1] + ": " + tokens[3] + "\n";
                }
            }
            
            finished = true;
            in.close();
        } catch (MalformedURLException ex) {
            System.err.println("Malformed URL when scraping kill data.");
            System.err.println(ex.getMessage());
        } catch (IOException ex) {
            System.err.println("IOException when scraping for kill data.");
            System.err.println(ex.getMessage());
        }
        
        if (finished) writeOut("temp_kills.txt", tempKillData);
    }
    
    private static void writeOut(String filename, String body) {
        try {
            Files.write(Paths.get(filename), body.getBytes());
        } catch (IOException ex) {
            System.err.println("IOException when writing to " + filename);
            System.err.println(ex.getMessage());
        }
    }
}
