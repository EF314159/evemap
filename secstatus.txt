// color gradient from -1.0 to 0.0
Nullsec:	0.5, 0.0, 0.5;	1.0, 0.0, 0.0
// color gradient from 0.0 to 0.5
Lowsec:		0.0, 1.0, 0.5;	1.0, 1.0, 0.5
// color gradient from 0.5 to 1.0
Highsec:	0.0, 0.5, 1.0;	0.0, 1.0, 1.0
